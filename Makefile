help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?# "}; {printf "\033[36m%-40s\033[0m %s\n", $$1, $$2}'

check-dependencies: ## Check dependencies
	# Checking dependency: helm
	@helm version

	# Checking dependency: kubectl
	@kubectl version --client

install-argocd: ## Install argocd
	@helm install argocd argo/argo-cd --version 7.1.3 --namespace argocd --create-namespace

get-argo-pw: ## get argocd pw
	@kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

argo-porforward: ## port forward argocd
	@kubectl port-forward service/argocd-server -n argocd 8080:443